export class Voiture{
    matricule:string;
    modele:string;
    idMarque:number;
    idTypevoiture:number;
    photo:string;
    id:number;
    prixLocation:number;    
    libelleMarque:string;
    libelleTypevoiture:string;

} 