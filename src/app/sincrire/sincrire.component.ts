import { Component, OnInit } from '@angular/core';
import { Client } from '../shared/client.model';
import { NgForm } from '@angular/forms';
import { ClientService } from '../client.service';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sincrire',
  templateUrl: './sincrire.component.html',
  styleUrls: ['./sincrire.component.css']
})
export class SincrireComponent implements OnInit {

  client:Client;
  emailPattern="^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$";
  
  constructor(private clientService:ClientService,private router: Router) { }

  ngOnInit() {
    this.resetForm();
  }
  resetForm(form?:NgForm){
    if(form!=null)
    form.reset();
    this.client={
      nom:'',
      prenom:'',
      email:'',
      motdepasse:'',
      CIN:'',
      adresse:'',
      telephone:'',
      etatCompte:0

    }
  }
  Enregsitrement(form:NgForm){
    this.clientService.EnregistrementClient(form.value)
    .subscribe((data:any)=>{
      console.log(data);
      if(data.success==true){

        this.resetForm(form);
        swal("Inscription Reussie!", "Inscription Reussie!", "success");
        this.router.navigate(['login']);
      }
      else{
        swal("Inscription Echoue!", "Inscription Echoue!", "warning");
      
      }
    })
  }

}
