import { Component, OnInit } from '@angular/core';
import { ReservationService } from '../reservation.service';
import { Reservation } from '../shared/reservation.model';
import { VoitureService } from '../voiture.service';

import { Router } from '@angular/router';
@Component({
  selector: 'app-mesreservations',
  templateUrl: './mesreservations.component.html',
  styleUrls: ['./mesreservations.component.css']
})
export class MesreservationsComponent implements OnInit {

  reservations:Object;
  idClient:string =localStorage.getItem('idUser');
  Voiture:Object ;
  
  msg_Connexion:string='SE CONNECTER';
  msg_Inscription:string="S'INSCRIRE";
  url_Inscription:string="sincrire"
  url_Connexion:string="login";  

  prenom:string="";
  nom:string="";

  
  constructor(public reservationsServices:ReservationService,private router: Router) { 

    this.reservationsServices.getMesReservations(this.idClient).subscribe(data => {

      //   console.log(data);
         this.reservations=data;    
         console.log(this.reservations);
       });
    }
 // getData(){
 /* this.reservationsServices.getMesReservations(this.idClient)
    .subscribe((data)=>{
      console.log(data);
      this.reservations=data;
      alert(data);
    })*/

   // this.reservationsServices.getMesReservations(this.idClient).subscribe(data => {

      //   console.log(data);
   //      this.reservations=data;    
     //    console.log(this.reservations);
     //  });
  //};
  ngOnInit() {
    
    
      if(localStorage.getItem('idUser')==null){
        this.router.navigate(['home']);
        return "";
  }else{
    this.prenom=localStorage.getItem('prenomClient');
    this.nom=localStorage.getItem('nomClient');
    this.msg_Inscription='SE DECONNECTER';
    this.msg_Connexion="MES RESERVATIONS";

    //A modifier apres pour y mettre la liste de mes Reservations
  this.url_Inscription="deconnexion"
  this.url_Connexion="";  
  }
  //this.getData();
    
    
  }

}
