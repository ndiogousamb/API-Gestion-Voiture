import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MesreservationsComponent } from './mesreservations.component';

describe('MesreservationsComponent', () => {
  let component: MesreservationsComponent;
  let fixture: ComponentFixture<MesreservationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesreservationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MesreservationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
