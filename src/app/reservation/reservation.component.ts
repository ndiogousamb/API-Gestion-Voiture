import { Component, OnInit } from '@angular/core';
import { Client } from '../shared/client.model';
import { Reservation } from '../shared/reservation.model';
import { NgForm } from '@angular/forms';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import swal from 'sweetalert2';
import { OnChanges, SimpleChanges, Input} from '@angular/core';
import { ReservationService } from '../reservation.service';
import { Router } from '@angular/router';

//main entry point


@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})

export class ReservationComponent implements OnInit , OnChanges {
 
  ngOnChanges(changes: SimpleChanges): void {
    
  }
  reservation:Reservation;
  
  constructor(private reservationService:ReservationService,private router: Router) { }
   d: Date = new Date();
   prixLoc:any=localStorage.getItem("prixLocation");
   photo:string=localStorage.getItem("photoVoiture");
   modele:string=localStorage.getItem("modeleVoiture");
   idUser:string=localStorage.getItem("idUser");
   idvoiture:string=localStorage.getItem("idVoiture");
   marque:string=localStorage.getItem("typevoiture");
  typevoiture:string=localStorage.getItem("marquevoiture");
  

  msg_Connexion:string='SE CONNECTER';
  msg_Inscription:string="S'INSCRIRE";
  url_Inscription:string="sincrire"
  url_Connexion:string="login";  
  prenom:string="";
  nom:string="";

   
  ngOnInit() {
    if(localStorage.getItem('idUser')==null){
      this.router.navigate(['login']);
      return "";
    }else{
      this.prenom=localStorage.getItem('prenomClient');
      this.nom=localStorage.getItem('nomClient');
      this.msg_Inscription='SE DECONNECTER';
      this.msg_Connexion="MES RESERVATIONS";
      this.url_Inscription="deconnexion"
      this.url_Connexion="mesreservations";  
    }


    this.Actualiseur();
   localStorage.getItem("prixLocation");
   localStorage.getItem("photoVoiture");
   localStorage.getItem("modeleVoiture");

   localStorage.getItem("idUser");
   this.idvoiture=localStorage.getItem("idVoiture");
  }
  Actualiseur(form?:NgForm){
    if(form!=null)
    form.reset();
   
    this.reservation={
      codeReservation:'',
      dateDebutLocate:'',
      dateFinLocation:'',
      montantLocation:0,
      idClient:this.idUser,
      idVoiture:this.idvoiture,
      nbjour:''
    }
  }

  Enregsitrement(form:NgForm){
    alert(" Deb Loc:"+this.reservation.dateDebutLocate +"Fin Loc: "+ this.reservation.dateFinLocation
    +" Mon Loc"+ this.reservation.montantLocation +" Id Voit"+ this.reservation.idVoiture
    +" id Client"+ this.reservation.idClient);
    this.reservationService.EnregistrementReservation(this.reservation)
    .subscribe((data:any)=>{
      console.log(data);
      if(data.success==true){

        this.Actualiseur(form);
        swal("Reservation Enregistree!", "Reservation Enregistree!", "success");
        
      }
      else{
        swal("Reservation Echoue!", "Reservation Echoue!", "warning");
      
      }
    })
  }

  calculMontantReservation(prixLoc, nbjour){
   // alert(this.reservation.nbjour);
   // alert(this.prixLoc);
    if(this.reservation.nbjour!=0)
    this.reservation.montantLocation=nbjour*prixLoc;
    alert(this.reservation.montantLocation);
  }
}
