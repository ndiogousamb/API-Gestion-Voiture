import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { SincrireComponent } from './sincrire/sincrire.component';
import { FormsModule} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { VoitureService } from './voiture.service';
import { HomeComponent } from './home/home.component';
import { ClientService } from './client.service';
import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { ReservationComponent } from './reservation/reservation.component';


import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {CdkTableModule} from '@angular/cdk/table';
import {HttpModule} from '@angular/http';
import { ReservationService } from './reservation.service';
import { DeconnexionComponent } from './deconnexion/deconnexion.component';
import { MesreservationsComponent } from './mesreservations/mesreservations.component';


const appRoutes:Routes = [
{
  path: 'login',
  component: LoginComponent },
{
  path: 'sincrire',
  component: SincrireComponent },
  {
    path: 'home',
    component: HomeComponent },
    {
      path: 'reservation',
      component: ReservationComponent },
      {
        path: 'deconnexion',
        component: DeconnexionComponent },
        {
          path: 'mesreservations',
          component: MesreservationsComponent },
  {
    path: '', redirectTo: 'home',
    pathMatch : 'full' },
]

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SincrireComponent,
    HomeComponent,
    ReservationComponent,
    DeconnexionComponent,
    MesreservationsComponent,
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    HttpClientModule,
    SweetAlert2Module.forRoot(),
   

    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
   
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    HttpClientModule,

   
  ],
  providers: [VoitureService,ClientService,ReservationService],
  bootstrap: [AppComponent]
})
export class AppModule implements OnInit {
  
  
  ngOnInit(){
    
  }
  
}
