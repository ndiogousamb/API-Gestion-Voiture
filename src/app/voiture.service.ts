import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class VoitureService {

   public apiURL='http://127.0.0.1:8000/api/voitures';
  constructor(private http: HttpClient) { }

  getData() {
     return this.http.get(this.apiURL);
     } 
}
