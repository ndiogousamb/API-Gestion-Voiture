import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Response } from '@angular/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import { Client } from './shared/client.model';
@Injectable()
export class ClientService {
    constructor(private http:HttpClient){

    }
    EnregistrementClient(client:Client){
        // alert(client.nom +" "+ client.prenom 
        // +" "+ client.email +" "+ client.motdepasse 
        // +" "+ client.adresse +" "+ client.CIN
        // +"  "+ client.telephone+" "+ client.etatCompte);
        
        return this.http.post('http://localhost:8000/api/clients/',{
        "nom": client.nom,
        "prenom": client.prenom,
        "email": client.email,
        "motdepasse": client.motdepasse,
        "telephone": client.telephone,
        "adresse": client.adresse,
        "CIN": client.CIN,
    });
    }
    Login(client:Client){      
        return this.http.get('http://localhost:8000/api/clients/loginCheck/'+""+client.email+"/"+client.motdepasse);
    }
}