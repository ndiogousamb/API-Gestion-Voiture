import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { VoitureService } from '../voiture.service';
import {Voiture} from '../shared/voiture.model';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent  implements OnInit{
  msg_Connexion:string='SE CONNECTER';
  msg_Inscription:string="S'INSCRIRE";
  url_Inscription:string="sincrire"
  url_Connexion:string="login";  
  prenom:string="";
  nom:string="";
 

  ngOnInit(): void {
    
    //localStorage.clear();
    if(localStorage.getItem('idUser')==null){
    //  alert(('Doit se connecter'));
      this.msg_Connexion='SE CONNECTER';
    this.msg_Inscription="S'INSCRIRE";
    this.url_Inscription="sincrire"
    this.url_Connexion="login";  
    }else{
      this.prenom=localStorage.getItem('prenomClient');
      this.nom=localStorage.getItem('nomClient');
      this.msg_Inscription='SE DECONNECTER';
      this.msg_Connexion="MES RESERVATIONS";

      //A modifier apres pour y mettre la liste de mes Reservations
    this.url_Inscription="deconnexion"
    this.url_Connexion="mesreservations";  
    }
    

   
  }



  seDeconnecter(){
   // alert('Deconnexion');
    if(localStorage.getItem('idUser')!=null){
    localStorage.clear();
  }

  }
  

  Voiture:Object ;
  louerVoiture(v:Voiture,libelleTypevoiture,libelleMarque){
    if(localStorage.getItem('idUser')==null){
      swal({
        type: 'error',
        title: 'Oops...',
        text: 'Vous devez vous Connecter avant de pouvoir procéder à une Location',
        footer: '',
      })
      this.router.navigate(['login']);
      return "";
    }
    else{
    localStorage.setItem("idVoiture",v.id+"");
    alert(localStorage.getItem("idVoiture"));
    localStorage.setItem("prixLocation",v.prixLocation+"");
    localStorage.setItem("photoVoiture",v.photo+"");
    localStorage.setItem("modeleVoiture",v.modele+"");
    localStorage.setItem("typevoiture",v.libelleTypevoiture);
    localStorage.setItem("marquevoiture",v.libelleMarque);
    alert(localStorage.getItem('idVoiture'));
    alert(localStorage.getItem('prixLocation'));
    this.router.navigate(['reservation']);
  }
    
  }
  constructor(public voiture:VoitureService,private router: Router) { 
    this.voiture.getData().subscribe(data => {

   //   console.log(data);
      this.Voiture=data;    
      console.log(this.Voiture);
    });
    
  }

 
  
}
