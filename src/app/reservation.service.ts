import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Response } from '@angular/http';

import { map } from 'rxjs/operators';
import { Reservation } from './shared/reservation.model';
@Injectable()
export class ReservationService {
  constructor(private http: HttpClient) {
  }

  EnregistrementReservation(reservation:Reservation){
  /*  alert(" Deb Loc:"+reservation.dateDebutLocate +"Fin Loc: "+reservation.dateFinLocation
    +" Mon Loc"+ reservation.montantLocation +" Id Voit"+reservation.idVoiture
    +" id Client"+ reservation.idClient);
    alert("ok");*/
    return this.http.post('http://localhost:8000/api/reservations/',{
    "dateDebutLocate": reservation.dateDebutLocate,
    "dateFinLocation": reservation.dateFinLocation,
    "montantLocation": reservation.montantLocation,
    "idClient": reservation.idClient,
    "codeReservation": "RESERVATION00006",
    "idVoiture": reservation.idVoiture
});
}

getMesReservations(id){
  return this.http.get('http://localhost:8000/api/reservations/all/'+id);
}

genererCodeReservation(){
  var resultat= this.http.get('http://localhost:8000/api/reservations/generateurcode');
 // var splitted = resultat.split(":", 2); 
}
}
