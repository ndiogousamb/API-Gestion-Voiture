import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { HttpClientModule } from '@angular/common/http';
import { HttpClient } from '@angular/common/http'
import { NgForm } from '@angular/forms';
import { ClientService } from '../client.service';
import { Client } from '../shared/client.model';
import swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  client: Client;
  constructor(private router: Router, private http: HttpClient, private clientService: ClientService) { }

  ngOnInit() {
  
    this.resetForm();
  }
  resetForm(form?: NgForm) {
    if (form != null)
      form.reset();
      this.client={
        nom:'',
        prenom:'',
        email:'',
        motdepasse:'',
        CIN:'',
        adresse:'',
        telephone:'',
        etatCompte:0
  
      }
  }



            clientConnexion(form: NgForm) {
              var test=0;
              this.clientService.Login(form.value)
                .subscribe((data: any) => {
                  
                  console.log(data);
               //  var jsonData =JSON.stringify(data);
               //  console.log(jsonData);
                  if(data.success==true){
                    this.resetForm(form);
                    swal("Connexion Reussie!", "Connexion Reussie!", "success");
                    localStorage.setItem('idUser',data.data.id);
                    localStorage.setItem('nomClient',data.data.nom);
                    localStorage.setItem('prenomClient',data.data.prenom);
                    alert(localStorage.getItem('idUser'));
                    alert(localStorage.getItem('nomClient'));
                    this.router.navigate(['home']);
                    return "";
                  }
                  else{
                    swal("Connexion Echoue!", "Connexion Echoue!", "warning");
                    return "";
                  }
                });
                          
          }
}

